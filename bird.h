#ifndef BIRD_H
#define BIRD_H

#include <QWidget>
#include <QPainter>


/// A class to represent birds.
///
/// Bird is a subclass of QWidget. This means that the following QWidget functions
/// are also available for objects of the class Bird:
/// * move(int x, int y): to move the bird to position (x,y)
/// * x(), y(): get the x position, get the y() position
/// * hide(): to hide a bird that has been painted

class Bird : public QWidget
{
    Q_OBJECT
public:
    ///
    /// Enum type for the EyeBrow
    ///
    enum EyeBrowType {
        UNI,    /**< enum value 0 */
        ANGRY,  /**< enum value 1 */
        UPSET,  /**< enum value 2 */
        BUSHY   /**< enum value 3 */
    };
    
    /// \fn Bird::Bird(QWidget *parent)
    /// \~English
    /// \brief Default constructor. The properties of the bird are set as follows:
    /// size: set to random value of either 50, 100, 150 or 200
    /// * eyeBrow: set randomly to one of the five possible values
    /// * color and eColor: set randomly to one of the five possible colors:
    /// "green", "blue", "red", "yellow", "white"
    /// \~Spanish
    /// \brief Constructor por defecto. Las propiedades de el pajaron son ajustados como sigue:
    /// * eyeBrow: ajustado aleatoriamente a uno de los 5 posibles valores
    /// * color y eColor: ajustao aleatoriamente a uno de los 5 posibles colores:
    /// "verde", "azul", "rojo", "amarillo", "blanco"
    ///
    explicit Bird(QWidget *parent = 0);

    /// \fn Bird::Bird(int s, EyeBrowType eb, QString col, QString eye, QWidget *parent)
    /// \~English
    /// \brief Constructor which accepts specification of the attributes of the bird
    /// \param s size
    /// \param eb eyeBrow
    /// \param col (faceColor)
    /// \param eye (eyeColor)
    /// \~Spanish
    /// \brief Constructor que acepta especificaciones de los atributos del pajaro
    /// \param s tamaño
    /// \param eb cejas
    /// \param col color de la cara
    /// \param eye color del ojo
    Bird(int , EyeBrowType , QString , QString, QWidget *parent = 0) ;

    /// \fn QString  Bird::getEyeColor()
    /// \~English
    /// \brief Getter for the eyeColor.
    /// \~Spanish
    /// \brief Devuelve el color del ojo.
    QString  getEyeColor() const;

    /// \fn QString  Bird::getFaceColor()
    /// \~English
    /// \brief Getter for the faceColor.
    /// \~Spanish
    /// \brief Devuelve el color de la cara.
    QString  getFaceColor() const;

    /// \fn void  Bird::setEyeColor(QString eye)
    /// \~English
    /// \brief Setter for the eyeColor.
    /// \param eye eyeColor
    /// \~Spanish
    /// \brief Ajusta el color del ojo
    /// \param eje color del ojo
    ///
    void  setEyeColor(QString) ;

    /// \fn void  Bird::setFaceColor(QString col)
    /// \~English
    /// \brief Setter for the face color.
    /// \param col face Color
    /// \~Spanish
    /// \brief Ajusta el color de la cara
    /// \param col color de la cara
    void  setFaceColor(QString) ;

    /// \fn int Bird::getSize()
    /// \~English
    /// \brief Getter for the bird size.
    /// \~Spanish
    /// \brief Devuelve el tamano del pajaro.
    int getSize() const;

    /// \fn int Bird::getEyeBrow()
    /// \~English
    /// \brief Getter for the bird eyeBrow type
    /// \~Spanish
    /// \brief Devuelve el tipo de ceja
    EyeBrowType getEyebrow() const ;

    /// \fn void  Bird::setSize(int s)
    /// \~English
    /// \brief Setter for the bird size.
    /// \param s bird size
    /// \~Spanish
    /// \brief Ajusta el tamano del pajaro
    /// \param s tamano del pajaro
    void setSize(int) ;

    /// \fn void  Bird::setEyebrow(QString eye)
    /// \~English
    /// \brief Setter for the eyebrow type.
    /// \param eb eyebrow type
    /// \~Spanish
    /// \brief Ajusta el tipo de cejas
    /// \param eb tipo de ceja
    void setEyebrow(EyeBrowType) ;

    /// \fn Qt::GlobalColor Bird::getColor(QString color)
    /// \~English
    /// \brief Converts a color specified as string to a Qt::GlobalColor
    /// \param color string with a color
    /// \~Spanish
    /// \brief Convierte un color especificado como un string a un Qt::GlobalColor
    /// \param color cadena de caracteres de un color
    Qt::GlobalColor getColor(QString) const;
signals:

public slots:

protected:
    /// \fn void Bird::paintEvent(QPaintEvent *event)
    /// \~English
    /// \brief This function is automatically invoked each time the widget or
    /// its parent receives a repaint signal.
    /// \~Spanish
    /// \brief Esta funcion es invocada automaticamente cada ves que el widget
    /// o su padre recive una senal de repintar.
    void paintEvent(QPaintEvent *event);

private:
    int size ; /**< size of the bird*/
    EyeBrowType eyeBrow ; /**< eyebrow type (ceja)*/
    QString color ; /**< face color  (cara)*/
    QString eColor ; /**< eye color (ojos)*/

    /// \fn int Bird::randInt(int min, int max)
    /// \~English
    /// \brief Generates a random integer in the range [min, max]
    /// \param min minimum random value
    /// \param max maximum random value
    /// \~Spanish
    /// \brief Genera un entero aleatorio en el rango [min, max]
    /// \param min valor aleatorio minimo
    /// \param max valor aleatorio maximo
    ///
    int randInt(int min, int max) ;
};

#endif // BIRD_H
